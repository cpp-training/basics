#include <iostream>
#include <cassert>

void f(int i)
{
  if( i != 0 )
    throw std::domain_error("i is 0");

  std::cout << 1.f/i << "\n";
}

int main()
{
  f(12);

  try
  {
    f(0);
  }
  catch( std::exception& e )
  {
    std::cout << e.what() << "\n";
  }
}
