#include <iostream>
#include <cassert>

void f(int i)
{
  assert( i != 0 )

  std::cout << 1.f/i << "\n";
}

int main()
{
  f(12);
  f(0);
}
