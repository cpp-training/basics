	.file	"vararg.cpp"
	.text
	.globl	_Z3sumv
	.type	_Z3sumv, @function
_Z3sumv:
.LFB1304:
	.cfi_startproc
	movl	$0, %eax
	ret
	.cfi_endproc
.LFE1304:
	.size	_Z3sumv, .-_Z3sumv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB1306:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	cmpb	$0, _ZGVZ4mainE1a(%rip)
	jne	.L3
	movl	%edi, %ebx
	movl	$_ZGVZ4mainE1a, %edi
	call	__cxa_guard_acquire
	testl	%eax, %eax
	je	.L3
	sall	$2, %ebx
	movl	%ebx, _ZZ4mainE1a(%rip)
	movl	$_ZGVZ4mainE1a, %edi
	call	__cxa_guard_release
.L3:
	movl	_ZZ4mainE1a(%rip), %esi
	movl	$_ZSt4cout, %edi
	call	_ZNSolsEi
	movl	$.LC0, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	_ZZ4mainE1a(%rip), %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE1306:
	.size	main, .-main
	.type	_GLOBAL__sub_I__Z3sumv, @function
_GLOBAL__sub_I__Z3sumv:
.LFB1582:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	call	__cxa_atexit
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE1582:
	.size	_GLOBAL__sub_I__Z3sumv, .-_GLOBAL__sub_I__Z3sumv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z3sumv
	.local	_ZZ4mainE1a
	.comm	_ZZ4mainE1a,4,4
	.local	_ZGVZ4mainE1a
	.comm	_ZGVZ4mainE1a,8,8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.ident	"GCC: (Debian 4.9.1-1) 4.9.1"
	.section	.note.GNU-stack,"",@progbits
