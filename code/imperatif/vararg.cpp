#include <iostream>

int sum()
{
  return 0;
}

template<typename T0, typename... Ts> auto sum(T0 v0, Ts... vs)
{
  return v0+sum(vs...);
}

int main(int argc, char** )
{
  static volatile auto a = sum(argc,argc,argc,argc);
  std::cout << a << "\n";
  return a;
}
