bool process_buffer(std::vector<MemoryBuffer> const& mems)
{
  return find_if( cbegin(mems), cend(mems)
                , [](MemoryBuffer const& mem)
                  {
                    return mem.size() > 2 && mem[0] == 'E' && mem[1] == 'Z';
                  }
                ) != cend(mems) ;
}
