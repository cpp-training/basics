#include <vector>

class stack : private std::vector<double>
{
  using parent = std::vector<double>;

  public:
  using parent::size;

  void push(double v)
  {
    parent::push_back(v);
  }

  double pop()
  {
    double v = parent::back();
    parent::pop_back();
    return v;
  }

  double top()
  {
    return parent::back();
  }
};
