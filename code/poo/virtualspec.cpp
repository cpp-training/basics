#include <iostream>

class base
{
  public:
  virtual void behavior() override {  std::cout << "b\n";  }
  virtual void foo() final {}
};

class derived final : public base
{
  public:
  virtual void behavior() override { std::cout << "d\n"; }
  void derived_behavior() {}
};

void process(base& b) { b.behavior(); }

int main()
{
  derived d;
  process(d);
}
