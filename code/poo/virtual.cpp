#include <iostream>

class base
{
  public:
  virtual void behavior() {  std::cout << "b\n";  }
};

class derived : public base
{
  public:
  virtual void behavior() { std::cout << "d\n"; }
  void derived_behavior() {}
};

void process(base& b) { b.behavior(); }

int main()
{
  derived d;
  process(d);
}
