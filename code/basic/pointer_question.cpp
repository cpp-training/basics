// Exercice : Donnez le type des variables ci-dessous

int               v1 = 1;
int*              v2 = nullptr;
int const         v3 = 1;
int const*        v4 = nullptr;
int* const        v5 = nullptr;
int const * const v6 = nullptr;
