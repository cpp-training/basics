#include <iostream>
#include <utility>
#include <tuple>

int main()
{
  std::pair<float,int> chu(3.f,5);
  float f = chu.first;
  int i = chu.second;

  std::tuple<int,char> foo (10,'x');
  auto bar = std::make_tuple ("test", 3.1, 14, 'y');

  std::get<2>(bar) = 100;

  int myint;
  char mychar;

  std::tie (myint, mychar) = foo;
  std::tie (std::ignore, std::ignore, myint, mychar) = bar;

  std::cout << "myint : " << myint << "\n";
  std::cout << "mychar: " << mychar << "\n";

  mychar = std::get<3>(bar);

  std::get<0>(foo) = std::get<2>(bar);
  std::get<1>(foo) = mychar;

  std::cout << "foo: ";
  std::cout << std::get<0>(foo) << ' ';
  std::cout << std::get<1>(foo) << '\n';

  std::size_t sz = std::tuple_size<decltype(foo)>::value;
  std::cout << sz << "\n";
}
