#define NORTH_WIND        0
#define SOUTH_WIND        1
#define EAST_WIND         2
#define WEST_WIND         3
#define NO_WIND           4

int wind_direction = NO_WIND;

enum wind_directions { NO_WIND, NORTH_WIND, SOUTH_WIND, EAST_WIND, WEST_WIND};

wind_directions w = NO_WIND;
wind_directions e = 453; // ERREUR

enum Color    { RED, GREEN, BLUE };
enum Feelings { EXCITED, MOODY, BLUE };

// Typage fort C++11
enum class Color    { RED, GREEN, BLUE };
enum class Feelings { EXCITED, MOODY, BLUE };

Color color = Color::GREEN;

// un entier 8 bits est suffisant ici
enum class Colors : unsigned char { RED = 1, GREEN = 2, BLUE = 3 };
