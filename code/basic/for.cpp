#include <iostream>
#include <vector>

int main()
{
  {
    auto ref = 1.;
    std::vector<decltype(ref)> dd(10);

    for(std::size_t i = 0;i<10;++i)
    {
      ref /= 10.;
      dd[i] = ref;
    }

     for(double& e : dd) std::cout << e << " ";
     std::cout << "\n";
  }

  {
    auto ref = 1.;
    std::vector<decltype(ref)> dd(10);

    for(auto it = dd.begin();it != dd.end();++it)
    {
      ref /= 10.;
      *it = ref;
    }

     for(double& e : dd) std::cout << e << " ";
     std::cout << "\n";
  }

  {
    auto ref = 1.;
    std::vector<decltype(ref)> dd(10);

    for(double& e : dd)
    {
      ref /= 10.;
      e = ref;
    }

    for(double& e : dd) std::cout << e << " ";
    std::cout << "\n";
  }
}
