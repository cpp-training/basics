#include <iostream>

int main()
{
  {
    float f;

    std::cin >> f;

    while(f > 1e-6f )
    {
      std::cout << f << "\n";
      f /= 10.f;
    }
  }

 {
    float f;

    std::cin >> f;

    do
    {
      std::cout << f << "\n";
      f /= 10.f;
    }
    while(f > 1e-6f );
  }
}
