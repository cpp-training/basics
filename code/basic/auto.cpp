#include <iostream>
#include <vector>
#include <type_traits>

#include <memory>
#include <string>
#include <cstdlib>




template <typename T> std::string type_name();


template <typename T> std::string type_name()
{
    typedef typename std::remove_reference<T>::type TR;

    std::string r = typeid(TR).name();

    if (std::is_const<TR>::value)
        r += " const";
    if (std::is_volatile<TR>::value)
        r += " volatile";
    if (std::is_lvalue_reference<T>::value)
        r += "&";
    else if (std::is_rvalue_reference<T>::value)
        r += "&&";
    return r;
}

int& foo_lref();
int&& foo_rref();
int foo_value();

int main()
{
  auto f = 3.f;
  auto s = "some C string";
  auto* p = &f;
  

  std::vector<double> dd = {1.,2e-1,1e-2,1e-3};
  auto  deci  = dd[1];
  auto& rdeci = dd[1];
  rdeci       = 1e-1;

  auto b = dd.begin();

  decltype(1/f + *b) z;

  auto l = {1,2,3,4,5};

  double i = 0;
  const int ci = 0;

  std::cout << "decltype(dd.begin()) is " << type_name<decltype(b)>() << '\n';
  std::cout << std::endl ;

  // Use : c++filt -t St16initializer_listIiE
  std::cout << "decltype(auto l = {1,2,3,4,5}) is " << type_name<decltype(l)>() << '\n';

  std::cout << "decltype(i) is " << type_name<decltype(i)>() << '\n';
  std::cout << "decltype((i)) is " << type_name<decltype((i))>() << '\n';
  std::cout << "decltype(ci) is " << type_name<decltype(ci)>() << '\n';
  std::cout << "decltype((ci)) is " << type_name<decltype((ci))>() << '\n';
  std::cout << "decltype(static_cast<int&>(i)) is " << type_name<decltype(static_cast<int&>(i))>() << '\n';
  std::cout << "decltype(static_cast<int&&>(i)) is " << type_name<decltype(static_cast<int&&>(i))>() << '\n';
  std::cout << "decltype(static_cast<int>(i)) is " << type_name<decltype(static_cast<int>(i))>() << '\n';
  std::cout << "decltype(foo_lref()) is " << type_name<decltype(foo_lref())>() << '\n';
  std::cout << "decltype(foo_rref()) is " << type_name<decltype(foo_rref())>() << '\n';
  std::cout << "decltype(foo_value()) is " << type_name<decltype(foo_value())>() << '\n';



  // decltype( dd[0] ) rd;
}
