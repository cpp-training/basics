#include <iostream>
#include <string>
#include <sstream>

int main()
{
  std::string values  = "125 320 512 750 333";
  std::istringstream iss(values);

  for(int n = 0; n < 5; n++)
  {
    int val;
    iss >> val;
    std::cout << val << std::endl;
  }

  std::ostringstream oss;

  oss << "a= " << 13.7 << "\tb= "<< 255 << "\tc=" << '#' << "\n";

  std::cout << oss.str();
}
