#include <vector>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

struct stat { int age; float poids, taille; };

int main()
{
  vector<stat> data;
  string ligne;

  ifstream fichier("data.txt" );

  while(getline(fichier, ligne))
  {
    int a;
    float p,t;

    istringstream src(ligne);
    src >> a >> p >> t;
    data.push_back( {a,p,t} );
  }
}
