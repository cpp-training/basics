#include <vector>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

struct stat { int age; float poids, taille; };

std::ostream& operator<<(std::ostream& os, stat const& s)
{
  os << s.age << s.poids << s.taille;
  return os
}

std::istream& operator>>(std::istream& is, stat& s)
{
  is >> s.age >> s.poids >> s.taille;
  return is
}

int main()
{
  string ligne;
  vector<stat> data;
  ifstream fichier("data.txt" );

  while(getline(fichier, ligne))
  {
    istringstream src(ligne);
    stat s;
    src >> s;
    data.push_back( s );
  }
}
