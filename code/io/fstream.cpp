#include <vector>
#include <fstream>

int main()
{
  std::vector<float> data = {1,2,3,4,5,6,7};

  std::ofstream txtfile("data.txt" );
  std::ofstream binfile("data.bin", std::ofstream::binary );

  for(auto& e : data)
    txtfile << e;

  binfile.write ( reinterpret_cast<const char*>(data.data())
                , data.size()*sizeof(float)
                );
}
