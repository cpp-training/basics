template<typename T> class allocator
{
  //...

  template<typename U> struct rebind { typedef allocator<U> other; };
};

// utilisation
allocator<T>::rebind<U>::other x;
