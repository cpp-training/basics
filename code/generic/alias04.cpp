// utilisation "a la typedef"

template<typename T> struct as_vector
{
  using type = std::vector<T>;
};

