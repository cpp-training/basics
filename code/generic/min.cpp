template<typename T, typename U> auto min(T a, U b) 
{ 
  return a < b ? a : b;
}

template<typename T> struct print;

int main()
{
  auto x = min(0,'c');
  print<decltype(x)> p;
}
