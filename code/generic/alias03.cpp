template<typename T> class allocator
{
  //...

  template<typename U> using rebind = allocator<U>;
};

// utilisation
allocator<T>::rebind<U> x;