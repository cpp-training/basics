#include <iostream>
#include <vector>
#include <algorithm>

// Copie multiples
std::vector<int> sort(std::vector<int> const& v)
{
  std::vector<int> that{v};

  std::sort( that.begin(), that.end() );

  return that;
}

// Interface détournée
void sort(std::vector<int> const& v, std::vector<int>& that)
{
  that = v;
  std::sort( that.begin(), that.end() );
}
