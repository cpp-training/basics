int a = 42;     // lvalue
int b = 43;     // lvalue

a = b;          // ok
b = a;          // ok
a = a * b;      // ok
int c = a * b;  // ok
a * b = 42;     // erreur

int getx() { return 17;}

const int& lcr = getx(); // ok
int& lr  = getx();       // erreur
