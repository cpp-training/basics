#include <iostream>
#include <typeinfo>

void foo(int const&)  { std::cout << "lvalue\n"; }
void foo(int&& x)       { std::cout << "rvalue\n"; }

int bar() { return 1337; }

int main()
{
  int  x = 3;
  int& y = x;
  int const& z = bar();

  foo(x);
  foo(y);
  foo(z);

  foo(4);
  foo(bar());
}
